

import React from 'react'

const useFetch = (url) => {


    const isMounted = React.useRef(true);


    const [state, setState] = React.useState({
        data: null,
        loading: true,
        error: null
    });

    React.useEffect(() => {

        return () => {
            isMounted.current = false;
        }
    }, []);


    React.useEffect(() => {

        setState({ data: null, loading: true, error: null });

        fetch(url)
            .then(resp => resp.json())
            .then(data => {

                if (isMounted.current) {
                    setState({
                        loading: false,
                        error: null,
                        data: data
                    });
                }


            })
            .catch(err => {
                if (isMounted.current) {
                    setState({
                        loading: false,
                        error: err,
                        data: null
                    });
                }
            })
    }, [url])

    return state;
}

export default useFetch
