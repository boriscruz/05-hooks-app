import React from 'react'

import MultpleCustomHooks from '../03-examples/MultpleCustomHooks';

import '../02-useEffect/effects.css';

const RealExampleRef = () => {

    const [show, setShow] = React.useState(false)

    return (
        <div>
            <h1>RealExampleRef</h1>
            <hr />

            {show && <MultpleCustomHooks />}

            <button
                className="btn btn-primary mt-5"
                onClick={
                    () => {
                        setShow(!show)
                    }
                }
            >
                Show/Hide
            </button>





        </div>
    )
}

export default RealExampleRef
