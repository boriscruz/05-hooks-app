import React from 'react';

import '../02-useEffect/effects.css';

const FocusScreen = () => {

    const inputRef = React.useRef();

    const handleClick = () => {
        inputRef.current.select();
    }

    return (
        <>
            <h1>Focus Screen</h1>
            <hr/>

            <input 
                ref={inputRef}
                className="form-control"
                placeholder="Your name"
                type="text"
            />

            <button 
                className="btn btn-outline-primary mt-5"
                onClick={handleClick}
            >
                Focus
            </button>

        </>
    )
}

export default FocusScreen
