import React from 'react'

import '../02-useEffect/effects.css'
import { useCounter } from '../../hooks/useCounter'
import { heavyProccess } from '../../helpers/heavyProccess';

const MemoHook = () => {

    const { counter, increment } = useCounter(100);

    const [show, setShow] = React.useState(true);

    const memoHeavyProccess = React.useMemo(() => heavyProccess(counter), [counter]);


    return (
        <div>
            <h2>MemoHook</h2>
            <h1>Counter: <small>{counter}</small> </h1>
            <hr />

            <p>{memoHeavyProccess}</p>

            <button
                className="btn btn-primary"
                onClick={increment}
            >
                +1
            </button>

            <button
                className="btn btn-primary"
                onClick={
                    () => {
                        setShow(!show);
                    }
                }
            >
                Show/Hide {JSON.stringify(show)}
            </button>

        </div>

    )
}

export default MemoHook

