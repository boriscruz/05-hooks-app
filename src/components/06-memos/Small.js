import React from 'react'

const Small = React.memo(({ value }) => {
    console.log('Component Small called');
    
    return (
        <div>
            <small>{value}</small>
        </div>
    )
})

export default Small
