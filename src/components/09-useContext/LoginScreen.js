import React from 'react'
import { UserContext } from './UserContext';

const LoginScreen = () => {

    const { setUser } = React.useContext(UserContext);

    const addUser = {
        id: new Date().getTime(),
        name: 'Rocio',
        email: 'roo@gmail.com'
    }

    return (
        <div>
            <h1>LoginScreen</h1>
            <hr />
            <button
                className="btn btn-primary"
                onClick={() => setUser(addUser)}
            >Login</button>
        </div>
    )
}

export default LoginScreen
