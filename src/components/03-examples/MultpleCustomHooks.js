import React from 'react'

import '../02-useEffect/effects.css'
import useFetch from '../../hooks/useFetch'
import { useCounter } from '../../hooks/useCounter'

const MultpleCustomHooks = () => {

    const { counter, increment, decrement } = useCounter(1);

    const { loading, data } = useFetch(`https://www.breakingbadapi.com/api/quotes/${counter}`);

    const { author, quote } = !!data && data[0];

    return (
        <div>
            <h1>BreakingBad Quotes</h1>
            <hr />

            {
                loading ?
                    (
                        <div className="alert alert-info text-center">
                            Loading
                        </div>
                    ) :
                    (
                        <blockquote className="blockquote text-right">
                            <p>{quote}</p>

                            <footer className="blockquote-footer">{author}</footer>

                        </blockquote>
                    )
            }

            <div className="text-right">
                <button
                    onClick={decrement}
                    className="btn btn-primary mr-5">
                    Previous quote
                </button>
                <button
                    onClick={increment}
                    className="btn btn-primary">
                    Next quote
                </button>
            </div>

        </div>
    )
}

export default MultpleCustomHooks
