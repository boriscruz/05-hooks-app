import React from 'react';

import './effects.css';
import { useForm } from '../../hooks/useForm';

const FormWithCustomHook = () => {

    const [formValues, handleInputChange] = useForm({
        name: '',
        email: '',
        password: ''
    });

    const { name, email, password } = formValues;

    const handleSubmit = (event) => {
        event.preventDefault();
        console.log(formValues);
    }

    return (
        <form onSubmit={handleSubmit}>
            <h1>FormWithCustomHook</h1>
            <hr />

            <div className="form-group">
                <input type="text"
                    name="name"
                    placeholder="Your Name"
                    autoComplete="off"
                    value={name}
                    onChange={handleInputChange}
                />
            </div>

            <div className="form-group">
                <input type="text"
                    name="email"
                    placeholder="Your email"
                    autoComplete="off"
                    value={email}
                    onChange={handleInputChange}
                />
            </div>

            <div className="form-group">
                <input type="password"
                    name="password"
                    placeholder="Your password"
                    autoComplete="off"
                    value={password}
                    onChange={handleInputChange}
                />
            </div>

            <button 
                type="submit"
                className="btn btn-primary"
            >Registrar</button>

        </form>
    )
}

export default FormWithCustomHook

