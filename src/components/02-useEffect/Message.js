import React from 'react'

const Message = () => {

    const [coords, setCoords] = React.useState({ x: 0, y: 0 });
    const { x, y } = coords;

    React.useEffect(() => {

        const mouseMove = (event) => {
            const coords = { x: event.x, y: event.y };
            console.log(coords);
            setCoords(coords);
        }

        window.addEventListener('mousemove', mouseMove);

        console.log('mount component');

        return () => {
            window.removeEventListener('mousemove', mouseMove);
        }
    }, []);

    return (
        <div>
            <h3>You're genius</h3>
            <p>
                x:{x} y:{y}
            </p>
        </div>
    )
}

export default Message
