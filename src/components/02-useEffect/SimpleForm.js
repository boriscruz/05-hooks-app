

import React from 'react';

import './effects.css';
import Message from './Message';

const SimpleForm = () => {

    const [formState, setFormState] = React.useState({
        name: '',
        email: ''
    });

    const { name, email } = formState;



    React.useEffect(() => {
    }, []);

    React.useEffect(() => {
    }, [formState]);

    React.useEffect(() => {
    }, [email]);

    const handleInputChange = ({ target }) => {
        setFormState({
            ...formState,
            [target.name]: target.value
        })
    }


    return (
        <>
            <h1>useEffect</h1>
            <hr />

            <div className="form-group">
                <input type="text"
                    name="name"
                    placeholder="Your Name"
                    autoComplete="off"
                    value={name}
                    onChange={handleInputChange}
                />
            </div>

            <div className="form-group">
                <input type="text"
                    name="email"
                    placeholder="Your email"
                    autoComplete="off"
                    value={email}
                    onChange={handleInputChange}
                />
            </div>

            {(name === '123' && <Message />)}


        </>
    )
}

export default SimpleForm

