import React from 'react'

import './layout.css';
import useFetch from '../../hooks/useFetch'
import { useCounter } from '../../hooks/useCounter'

const Layout = () => {

    const pTag = React.useRef();

    const { counter, increment, decrement } = useCounter(1);

    const { data } = useFetch(`https://www.breakingbadapi.com/api/quotes/${counter}`);

    const { quote } = !!data && data[0];

    const [boxSize, setBoxSize] = React.useState({})

    React.useLayoutEffect(() => {
        setBoxSize(pTag.current.getBoundingClientRect());
    }, [quote]);


    return (
        <div>
            <h1>BreakingBad Quotes</h1>
            <hr />

            <blockquote className="blockquote text-right">
                <p
                    ref={pTag}
                    className="mb-0"
                >{quote}</p>

            </blockquote>

            <pre>
                {JSON.stringify(boxSize, null, 3)}
            </pre>

            <div className="text-right">
                <button
                    onClick={decrement}
                    className="btn btn-primary mr-5">
                    Previous quote
                </button>
                <button
                    onClick={increment}
                    className="btn btn-primary">
                    Next quote
                </button>
            </div>

        </div>
    )
}

export default Layout
